package com.example.raktarkezelo.util;

public class StringUtils {

    public static boolean isEmptyOrNull(String str) {
        return str == null || str.isEmpty();
    }
}
