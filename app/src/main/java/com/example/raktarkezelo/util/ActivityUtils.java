package com.example.raktarkezelo.util;

import android.app.Activity;
import android.app.AlertDialog;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.example.raktarkezelo.R;
import com.google.firebase.auth.FirebaseUser;

public class ActivityUtils {

    public static void alert(Activity activity, int messageId) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setMessage(messageId);
        AlertDialog alert = builder.create();
        alert.show();
    }

    public static void alert(Activity activity, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setMessage(message);
        AlertDialog alert = builder.create();
        alert.show();
    }

    public static void setVisibility(TextView textView, Object field) {
        if (textView == null) {
            return;
        }
        if (field == null || (field instanceof String && ((String) field).isEmpty())) {
            textView.setVisibility(View.GONE);
        }
    }

    public static void validateUser(Activity activity, FirebaseUser user) {
        if (user == null) {
            alert(activity, R.string.invalid_user_msg);
            activity.finish();
        }
    }

    public static void colorMenuItem(MenuItem item, int color) {
        Drawable drawable = item.getIcon();
        if (drawable != null) {
            drawable.setColorFilter(color, PorterDuff.Mode.SRC_ATOP);
        }
    }
}
