package com.example.raktarkezelo.data;

public class Firm extends AbstractEntity {

    private String firmName;
    private String vatNumber;
    private String firmActivity;

    public String getFirmName() {
        return firmName;
    }

    public void setFirmName(String firmName) {
        this.firmName = firmName;
    }

    public String getVatNumber() {
        return vatNumber;
    }

    public void setVatNumber(String vatNumber) {
        this.vatNumber = vatNumber;
    }

    public String getFirmActivity() {
        return firmActivity;
    }

    public void setFirmActivity(String firmActivity) {
        this.firmActivity = firmActivity;
    }
}
