package com.example.raktarkezelo.data;

import java.io.Serializable;
import java.util.Date;

public abstract class TrackedEntity extends AbstractEntity implements Serializable {

    private Date created;
    private Date deleted;
    private Date lastEdit;

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getDeleted() {
        return deleted;
    }

    public void setDeleted(Date deleted) {
        this.deleted = deleted;
    }

    public Date getLastEdit() {
        return lastEdit;
    }

    public void setLastEdit(Date lastEdit) {
        this.lastEdit = lastEdit;
    }
}
