package com.example.raktarkezelo.data;

import java.io.Serializable;

public class Partner extends TrackedEntity implements Serializable {

    private PartnerType type;
    private String partnerIdentifier;
    private String name;
    private String address;
    private String taxNumber;
    private String phoneNumber;
    private String email;

    private String reference;

    public PartnerType getType() {
        return type;
    }

    public void setType(PartnerType type) {
        this.type = type;
    }

    public String getPartnerIdentifier() {
        return partnerIdentifier;
    }

    public void setPartnerIdentifier(String partnerIdentifier) {
        this.partnerIdentifier = partnerIdentifier;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTaxNumber() {
        return taxNumber;
    }

    public void setTaxNumber(String taxNumber) {
        this.taxNumber = taxNumber;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getNameWithRegNumber() {
        StringBuilder builder = new StringBuilder();
        builder.append(name);
        builder.append(" (");
        builder.append(partnerIdentifier);
        builder.append(")");
        return builder.toString();
    }

    @Override
    public String toString() {
       return getNameWithRegNumber();
    }

    public enum PartnerType {
        CUSTOMER,
        SUPPLIER;
    }
}


