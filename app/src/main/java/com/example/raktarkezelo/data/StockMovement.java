package com.example.raktarkezelo.data;

import com.google.firebase.firestore.Exclude;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Objects;

public class StockMovement extends TrackedEntity implements Serializable {

    private MovementDirection movementDirection;
    private Date movementDate;
    private String storageReference;
    private Storage storage;
    private String productReference;
    private Product product;
    private String partnerReference;
    private Partner partner;
    private double quantity;

    public MovementDirection getMovementDirection() {
        return movementDirection;
    }

    public void setMovementDirection(MovementDirection movementDirection) {
        this.movementDirection = movementDirection;
    }

    public Date getMovementDate() {
        return movementDate;
    }

    public void setMovementDate(Date movementDate) {
        this.movementDate = movementDate;
    }

    public String getStorageReference() {
        return storageReference;
    }

    public void setStorageReference(String storageReference) {
        this.storageReference = storageReference;
    }

    @Exclude
    public Storage getStorage() {
        return storage;
    }

    @Exclude
    public void setStorage(Storage storage) {
        this.storage = storage;
        this.storageReference = storage.getReference();
    }

    public String getProductReference() {
        return productReference;
    }

    public void setProductReference(String productReference) {
        this.productReference = productReference;
    }

    @Exclude
    public Product getProduct() {
        return product;
    }

    @Exclude
    public void setProduct(Product product) {
        this.product = product;
        this.productReference = product.getReference();
    }

    public String getPartnerReference() {
        return partnerReference;
    }

    public void setPartnerReference(String partnerReference) {
        this.partnerReference = partnerReference;
    }

    @Exclude
    public Partner getPartner() {
        return partner;
    }

    @Exclude
    public void setPartner(Partner partner) {
        this.partner = partner;
        this.partnerReference = partner.getReference();
    }

    @Exclude
    public BigDecimal getQuantityBigDecimal() {
        return MovementDirection.INWARD.equals(movementDirection)
                ? BigDecimal.valueOf(quantity)
                : BigDecimal.valueOf(quantity).negate();
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    @Exclude
    public GroupingKey getGroupingKey() {
        return new GroupingKey(storage, product);
    }

    //Készletmozgás iránya
    public enum MovementDirection {
        INWARD,
        OUTWARD
    }

    public class GroupingKey {
        private final Storage storage;
        private final Product product;

        public GroupingKey(Storage storage, Product product) {
            this.storage = storage;
            this.product = product;
        }

        public Storage getStorage() {
            return storage;
        }

        public Product getProduct() {
            return product;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            GroupingKey groupingKey = (GroupingKey) o;
            return storage.equals(groupingKey.storage) &&
                    product.equals(groupingKey.product);
        }

        @Override
        public int hashCode() {
            return Objects.hash(storage, product);
        }
    }
}
