package com.example.raktarkezelo.data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Objects;

public class StockView implements Serializable {

    public Date stockDate;
    public Storage storage;
    public Product product;
    //Raktáron levő mennyiség
    private BigDecimal quantity;

    public StockView(Date stockDate, Storage storage, Product product, BigDecimal quantity) {
        this.stockDate = stockDate;
        this.storage = storage;
        this.product = product;
        this.quantity = quantity;
    }

    public Date getStockDate() {
        return stockDate;
    }

    public Storage getStorage() {
        return storage;
    }

    public Product getProduct() {
        return product;
    }

    public BigDecimal getQuantity() {
        return quantity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StockView stockView = (StockView) o;
        return Objects.equals(storage, stockView.storage) &&
                product.equals(stockView.product);
    }

    @Override
    public int hashCode() {
        return Objects.hash(storage, product);
    }
}
