package com.example.raktarkezelo.data;

import java.io.Serializable;

public class Storage extends TrackedEntity implements Serializable {
    public String storageName;
    public String reference;

    public String getStorageName() {
        return storageName;
    }

    public void setStorageName(String storageName) {
        this.storageName = storageName;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    @Override
    public String toString() {
       return getStorageName();
    }
}
