package com.example.raktarkezelo.data;

import com.google.firebase.firestore.Exclude;

import java.io.Serializable;
import java.math.BigDecimal;

public class Product extends TrackedEntity implements Serializable {

    //Cikkszám
    private String productNumber;
    private String name;
    //Termékcsoport
    private String productCategory;
    //Mértékegység
    private String unit;
    //Vonalkód
    private String barcode;

    private String reference;

    private Double minimumStock;
    private Double maximumStock;

    public String getProductNumber() {
        return productNumber;
    }

    public void setProductNumber(String productNumber) {
        this.productNumber = productNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProductCategory() {
        return productCategory;
    }

    public void setProductCategory(String productCategory) {
        this.productCategory = productCategory;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public Double getMinimumStock() {
        return minimumStock;
    }

    @Exclude
    public BigDecimal getMinimumStockBigDecimal() {
        return BigDecimal.valueOf(minimumStock);
    }

    public void setMinimumStock(Double minimumStock) {
        this.minimumStock = minimumStock;
    }

    public Double getMaximumStock() {
        return maximumStock;
    }

    @Exclude
    public BigDecimal getMaximumStockBigDecimal() {
        return BigDecimal.valueOf(minimumStock);
    }

    public void setMaximumStock(Double maximumStock) {
        this.maximumStock = maximumStock;
    }

    @Exclude
    public String getNameWithProductNumber() {
        StringBuilder builder = new StringBuilder();
        builder.append(name);
        builder.append(" (");
        builder.append(productNumber);
        builder.append(")");
        return builder.toString();
    }

    @Override
    public String toString() {
        return getNameWithProductNumber();
    }
}
