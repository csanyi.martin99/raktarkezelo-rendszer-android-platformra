package com.example.raktarkezelo.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;

import com.example.raktarkezelo.R;
import com.example.raktarkezelo.data.Product;
import com.example.raktarkezelo.service.Callback;
import com.example.raktarkezelo.service.WarehouseRepository;
import com.example.raktarkezelo.service.WarehouseRepositoryImpl;
import com.example.raktarkezelo.util.ActivityUtils;

import java.math.RoundingMode;
import java.text.DecimalFormat;

import static com.example.raktarkezelo.service.FirebaseReference.AUTHENTICATION;
import static com.example.raktarkezelo.util.ActivityUtils.setVisibility;

public class ProductDetailActivity extends AppCompatActivity {

    private static final String LOG_TAG = ProductDetailActivity.class.getName();

    private Product mProduct;
    private WarehouseRepository mWarehouseRepository;
    private DecimalFormat mDecimalFormat;

    private TextView productNumberTv, productNameTv, productCategoryTv, productUnitTv,
            barCodeTv, minimumStockTv, maximumStockTv;
    private EditText productNumberEt, productNameEt, productCategoryEt, productUnitEt,
            barCodeEt, minimumStockEt, maximumStockEt;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_detail);
        ActivityUtils.validateUser(this, AUTHENTICATION.getCurrentUser());

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mWarehouseRepository = new WarehouseRepositoryImpl(this);
        mDecimalFormat = new DecimalFormat("###,###.##");
        mDecimalFormat.setRoundingMode(RoundingMode.HALF_UP);

        bindViewComponents();
        loadOrCreateProduct();
        setTextViewVisibility();
    }

    private void bindViewComponents() {
        productNumberEt = findViewById(R.id.productNumberEt);
        productNameEt = findViewById(R.id.productNameEt);
        productCategoryEt = findViewById(R.id.productCategoryEt);
        productUnitEt = findViewById(R.id.productUnitEt);
        barCodeEt = findViewById(R.id.barCodeEt);
        minimumStockEt = findViewById(R.id.minimumStockEt);
        maximumStockEt = findViewById(R.id.maximumStockEt);
    }

    private void loadOrCreateProduct() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            mProduct = (Product) extras.get(StockListActivity.PARAM_PRODUCT);
        }
        if (mProduct == null) {
            mProduct = new Product();
        } else {
            loadProduct();
        }
    }

    private void loadProduct() {
        mWarehouseRepository.getProductById(mProduct.getId(), new Callback() {
            @Override
            public void onSuccess(Object object) {
                setViewComponents();
            }

            @Override
            public void onError(Object object) {
            }
        });
    }

    private void setViewComponents() {
        productNumberEt.setText(mProduct.getProductNumber());
        productNameEt.setText(mProduct.getName());
        productCategoryEt.setText(mProduct.getProductCategory());
        productUnitEt.setText(mProduct.getUnit());
        barCodeEt.setText(mProduct.getBarcode());
        if (mProduct.getMinimumStock() != null) {
            minimumStockEt.setText(mDecimalFormat.format(mProduct.getMinimumStock()));
        }
        if (mProduct.getMaximumStock() != null) {
            maximumStockEt.setText(mDecimalFormat.format(mProduct.getMaximumStock()));
        }
    }

    private void setTextViewVisibility() {
        productNumberTv = findViewById(R.id.productNumberTv);
        setVisibility(productNumberTv, mProduct.getProductNumber());
        productNameTv = findViewById(R.id.productNameTv);
        setVisibility(productNameTv, mProduct.getName());
        productCategoryTv = findViewById(R.id.productCategoryTv);
        setVisibility(productCategoryTv, mProduct.getProductCategory());
        productUnitTv = findViewById(R.id.productUnitTv);
        setVisibility(productUnitTv, mProduct.getUnit());
        barCodeTv = findViewById(R.id.barCodeTv);
        setVisibility(barCodeTv, mProduct.getBarcode());
        minimumStockTv = findViewById(R.id.minimumStockTv);
        setVisibility(minimumStockTv, mProduct.getMinimumStock());
        maximumStockTv = findViewById(R.id.maximumStockTv);
        setVisibility(maximumStockTv, mProduct.getMaximumStock());

        getSupportActionBar().setTitle(mProduct.isNewEntity()
                ? R.string.create_product
                : R.string.edit_product);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        if (mProduct == null || mProduct.isNewEntity()) {
            MenuItem deleteMenuItem = menu.findItem(R.id.delete_menu_item);
            deleteMenuItem.setVisible(false);
        }
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.modify_delete_menu, menu);
        ActivityUtils.colorMenuItem(menu.findItem(R.id.back_menu_item), Color.BLACK);
        ActivityUtils.colorMenuItem(menu.findItem(R.id.delete_menu_item), Color.BLACK);
        ActivityUtils.colorMenuItem(menu.findItem(R.id.save_menu_item), Color.BLACK);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.back_menu_item) {
            finish();
            return true;
        }

        if (item.getItemId() == R.id.delete_menu_item) {
            deleteProduct();
            return true;
        }

        if (item.getItemId() == R.id.save_menu_item) {
            saveProduct();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void saveProduct() {
        boolean fieldsRequired = checkRequiredFields();
        if (!fieldsRequired) {
            return;
        }
        setProduct();
        mWarehouseRepository.saveProduct(mProduct, new Callback() {
            @Override
            public void onSuccess(Object object) {
                finish();
            }

            @Override
            public void onError(Object object) {
                ActivityUtils.alert(ProductDetailActivity.this, R.string.product_save_error);
            }
        });
    }

    private boolean checkRequiredFields() {
        if (productNameEt.getText().length() == 0 || productNumberEt.getText().length() == 0) {
            ActivityUtils.alert(this, R.string.product_save_constraint_message);
            return false;
        }
        return true;
    }


    private void setProduct() {
        mProduct.setProductNumber(productNumberEt.getText().toString());
        mProduct.setName(productNameEt.getText().toString());
        mProduct.setProductCategory(productCategoryEt.getText().toString());
        mProduct.setUnit(productUnitEt.getText().toString());
        mProduct.setBarcode(barCodeEt.getText().toString());
        if (minimumStockEt.getText().length() != 0) {
            mProduct.setMinimumStock(Double.valueOf(minimumStockEt.getText().toString()));
        }
        if (maximumStockEt.getText().length() != 0) {
            mProduct.setMaximumStock(Double.valueOf(maximumStockEt.getText().toString()));
        }
    }

    private void deleteProduct() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.delete_product_msg)
                .setTitle(R.string.delete)
                .setCancelable(false)
                .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                })
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        mWarehouseRepository.deleteProduct(mProduct, new Callback() {
                            @Override
                            public void onSuccess(Object object) {
                                finish();
                            }

                            @Override
                            public void onError(Object object) {
                                ActivityUtils.alert(ProductDetailActivity.this, R.string.product_delete_error);
                            }
                        });
                    }
                });

        builder.create().show();
    }


}