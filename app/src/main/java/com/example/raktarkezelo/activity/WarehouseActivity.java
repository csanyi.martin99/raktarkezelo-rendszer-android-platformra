package com.example.raktarkezelo.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.example.raktarkezelo.R;
import com.example.raktarkezelo.activity.StockListActivity;
import com.example.raktarkezelo.activity.StockMovementActivity;
import com.example.raktarkezelo.util.ActivityUtils;

public class WarehouseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_warehouse);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.warehouse);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.back_menu, menu);
        ActivityUtils.colorMenuItem(menu.findItem(R.id.back_menu_item), Color.BLACK);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.back_menu_item) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void stockButton(View view) {
        Intent stockpageIntent = new Intent(this, StockListActivity.class);
        startActivity(stockpageIntent);
    }

    public void stockMovementButton(View view) {
        Intent stockMovementIntent = new Intent(this, StockMovementActivity.class);
        startActivity(stockMovementIntent);
    }
}