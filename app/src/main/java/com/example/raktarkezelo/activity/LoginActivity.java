package com.example.raktarkezelo.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.example.raktarkezelo.R;
import com.example.raktarkezelo.util.ActivityUtils;
import com.example.raktarkezelo.util.StringUtils;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;

import java.util.Locale;

import static com.example.raktarkezelo.service.FirebaseReference.AUTHENTICATION;

public class LoginActivity extends AppCompatActivity {

    private static final String LOG_TAG = LoginActivity.class.getName();

    private EditText emailET;
    private EditText passwordET;

    private ImageView localeFlag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        localeFlag = findViewById(R.id.localeFlag);

        emailET = findViewById(R.id.editTextUserName);
        passwordET = findViewById(R.id.editTextPassword);

        emailET.setText("csanyi.martin99@gmail.com");
        passwordET.setText("asdasd");
    }

    public void loginButton(View view) {
        String userNameStr = emailET.getText().toString();
        String passwordStr = passwordET.getText().toString();

        if (StringUtils.isEmptyOrNull(userNameStr) || StringUtils.isEmptyOrNull(passwordStr)) {
            ActivityUtils.alert(this, R.string.login_constraint_msg);
            return;
        }

        AUTHENTICATION.signInWithEmailAndPassword(userNameStr, passwordStr)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Log.i(LOG_TAG, "signInWithEmail:success");
                            Intent homepageIntent = new Intent(LoginActivity.this, HomepageActivity.class);
                            startActivity(homepageIntent);
                        } else {
                            Log.w(LOG_TAG, "signInWithEmail:failure", task.getException());
                            ActivityUtils.alert(LoginActivity.this, R.string.login_error_msg);
                        }
                    }
                });
    }


    public void onClickLanguage(View view) {
        setNewLocale();
//        restartActivity();
    }

    private void setNewLocale() {
        Configuration config = getBaseContext().getResources().getConfiguration();
        Locale currentLocale = getResources().getConfiguration().locale;
        if (new Locale("hu").equals(currentLocale)) {
            config.setLocale(Locale.ENGLISH);
        } else {
            config.setLocale(new Locale("hu"));
        }
        getBaseContext().getResources().updateConfiguration(config,
                getBaseContext().getResources().getDisplayMetrics());
        setLocaleFlag();
        restartActivity();
    }

    private void restartActivity() {
        Intent intent = getIntent();
        finish();
        startActivity(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        setLocaleFlag();
    }

    private void setLocaleFlag() {
        Locale currentLocale = getResources().getConfiguration().locale;
        if (new Locale("hu").equals(currentLocale)) {
            localeFlag.setImageResource(R.drawable.uk_flag);
        } else {
            localeFlag.setImageResource(R.drawable.hun_flag);
        }
    }
}