package com.example.raktarkezelo.activity.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.raktarkezelo.R;
import com.example.raktarkezelo.activity.OnItemClickListener;
import com.example.raktarkezelo.data.StockMovement;

import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.List;

import static com.example.raktarkezelo.data.StockMovement.MovementDirection.INWARD;

public class StockMovementItemAdapter extends RecyclerView.Adapter<StockMovementItemAdapter.ViewHolder> {

    private static final String LOG_TAG = StockMovementItemAdapter.class.getName();

    private final Activity mContext;
    private final List<StockMovement> mStockMovementList;
    private OnItemClickListener mOnItemClickListener;

    public StockMovementItemAdapter(Activity context, List<StockMovement> stockMovementList, OnItemClickListener onItemClickListener) {
        this.mContext = context;
        this.mStockMovementList = stockMovementList;
        this.mOnItemClickListener = onItemClickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext)
                .inflate(R.layout.stock_movement_listitem, parent, false);
        return new ViewHolder(view, mOnItemClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        StockMovement movement = mStockMovementList.get(position);
        holder.bindTo(movement);
    }

    @Override
    public int getItemCount() {
        return mStockMovementList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView smDateTv, smProductTv, smStorageTv, smChangeTv;
        private OnItemClickListener onItemClickListener;
        private DecimalFormat mDecimalFormatter;
        private DateFormat mDateFormat;

        public ViewHolder(@NonNull View itemView, OnItemClickListener listener) {
            super(itemView);

            smDateTv = itemView.findViewById(R.id.smDateTv);
            smProductTv = itemView.findViewById(R.id.smProductTv);
            smStorageTv = itemView.findViewById(R.id.smStorageTv);
            smChangeTv = itemView.findViewById(R.id.smChangeTv);

            onItemClickListener = listener;
            mDecimalFormatter = new DecimalFormat("###,###.##");
            mDecimalFormatter.setRoundingMode(RoundingMode.HALF_UP);
            mDateFormat = new SimpleDateFormat("yyyy.MM.dd. hh:mm");

            itemView.setOnClickListener(this);
        }

        public void bindTo(StockMovement movement) {
            smDateTv.setText(mDateFormat.format(movement.getMovementDate()));
            smProductTv.setText(movement.getProduct().getNameWithProductNumber());
            smStorageTv.setText(movement.getStorage().getStorageName());
            smChangeTv.setText(getChange(movement));
        }

        private String getChange(StockMovement movement) {
            StringBuilder sb = new StringBuilder();
            sb.append(INWARD.equals(movement.getMovementDirection()) ? "+" : "-");
            sb.append(mDecimalFormatter.format(movement.getQuantity()));
            sb.append(" ");
            if (movement.getProduct().getUnit() != null) {
                sb.append(movement.getProduct().getUnit());
            }
            return sb.toString();
        }

        @Override
        public void onClick(View v) {
            onItemClickListener.onItemClick(getAdapterPosition());
        }
    }

}
