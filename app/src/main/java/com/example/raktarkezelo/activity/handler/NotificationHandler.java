package com.example.raktarkezelo.activity.handler;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import androidx.core.app.NotificationCompat;

import com.example.raktarkezelo.R;
import com.example.raktarkezelo.activity.StockListActivity;

public class NotificationHandler {

    private static final String CHANNEL_ID = "stock_notification_channel";
    private static final int NOTIFICATION_ID = 0;

    private NotificationManager mManager;
    private Context mContext;

    public NotificationHandler(Context mContext) {
        this.mContext = mContext;
        this.mManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
        createChannel();
    }

    //NotificationChannel létrehozása
    private void createChannel() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            return;
        }

        NotificationChannel channel = new NotificationChannel(
                CHANNEL_ID,
                "Stock notification",
                NotificationManager.IMPORTANCE_DEFAULT);

        channel.enableLights(true);
        channel.enableVibration(true);
        mManager.createNotificationChannel(channel);
    }

    //üzenet küldése
    public void send(String title, String message) {
        Intent intent = new Intent(mContext, StockListActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(
                mContext, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(mContext, CHANNEL_ID)
                .setContentTitle(title)
                .setContentText(message)
                .setSmallIcon(R.drawable.ic_chat_bubble)
                .setContentIntent(pendingIntent);

        mManager.notify(NOTIFICATION_ID, builder.build());
    }

    //üzenet törlése
    public void cancel() {
        mManager.cancel(NOTIFICATION_ID);
    }
}
