package com.example.raktarkezelo.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.raktarkezelo.R;
import com.example.raktarkezelo.data.Partner;
import com.example.raktarkezelo.service.Callback;
import com.example.raktarkezelo.service.PartnerRepository;
import com.example.raktarkezelo.service.PartnerRepositoryImpl;
import com.example.raktarkezelo.util.ActivityUtils;

import static com.example.raktarkezelo.data.Partner.PartnerType.CUSTOMER;
import static com.example.raktarkezelo.service.FirebaseReference.AUTHENTICATION;
import static com.example.raktarkezelo.util.ActivityUtils.setVisibility;

public class ModifyPartnersSuppliersActivity extends AppCompatActivity {

    private static final String LOG_TAG = ModifyPartnersSuppliersActivity.class.getName();

    private Partner mPartner;
    private Partner.PartnerType mType = CUSTOMER;
    private PartnerRepository mPartnerRepository;

    private TextView identifierTv, nameTv, taxNumberTv, addressTv, phoneNumberTv, emailTv;
    private EditText identifierEt, nameEt, taxNumberEt, addressEt, phoneNumberEt, emailEt;
    private ImageView partnerImg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modify_partners_suppliers);
        ActivityUtils.validateUser(this, AUTHENTICATION.getCurrentUser());

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mPartnerRepository = new PartnerRepositoryImpl(this);
        bindViewComponents();
        loadOrCreatePartner();
        setTextViewVisibility();
    }

    //összekapcsolom a felület komponenseit a referenciákkal
    private void bindViewComponents() {
        identifierEt = findViewById(R.id.identifierEt);
        nameEt = findViewById(R.id.nameEt);
        taxNumberEt = findViewById(R.id.taxNumberEt);
        addressEt = findViewById(R.id.addressEt);
        phoneNumberEt = findViewById(R.id.phoneNumberEt);
        emailEt = findViewById(R.id.emailEt);
        partnerImg = findViewById(R.id.partnerImg);
    }

    //betöltöm a kiválasztott partnert, ha az "Új" gombbal jöttünk az activity-re, akkor készítek egy új partnert
    private void loadOrCreatePartner() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            mPartner = (Partner) extras.get(PartnersActivity.PARAM_PARTNER);
            mType = (Partner.PartnerType) extras.get(PartnersActivity.PARAM_PARTNER_TYPE);
        }
        if (mPartner == null) {
            mPartner = new Partner();
            mPartner.setType(mType);
        } else {
            loadPartner();
        }
    }

    private void loadPartner() {
        mPartnerRepository.getPartnerById(mPartner.getId(), new Callback() {
            @Override
            public void onSuccess(Object object) {
                setViewComponents();
            }

            @Override
            public void onError(Object object) {
            }
        });
    }

    //a mentett partnerből megpróbálok értéket adni a komponenseknek
    private void setViewComponents() {
        identifierEt.setText(mPartner.getPartnerIdentifier());
        nameEt.setText(mPartner.getName());
        taxNumberEt.setText(mPartner.getTaxNumber());
        addressEt.setText(mPartner.getAddress());
        phoneNumberEt.setText(mPartner.getPhoneNumber());
        emailEt.setText(mPartner.getEmail());
    }

    //Minden komponenshez tartozik egy textview is. Ezeket akkor jelenítem meg, ha egy komponenshez tartozik érték,
    //így mindig látja a felhasználó a komponens nevét.
    private void setTextViewVisibility() {
        identifierTv = findViewById(R.id.identifierTv);
        setVisibility(identifierTv, mPartner.getPartnerIdentifier());
        nameTv = findViewById(R.id.nameTv);
        setVisibility(nameTv, mPartner.getName());
        taxNumberTv = findViewById(R.id.taxNumberTv);
        setVisibility(taxNumberTv, mPartner.getTaxNumber());
        addressTv = findViewById(R.id.addressTv);
        setVisibility(addressTv, mPartner.getAddress());
        phoneNumberTv = findViewById(R.id.phoneNumberTv);
        setVisibility(phoneNumberTv, mPartner.getPhoneNumber());
        emailTv = findViewById(R.id.emailTv);
        setVisibility(emailTv, mPartner.getEmail());

        if (CUSTOMER.equals(mPartner.getType())) {
            getSupportActionBar().setTitle(mPartner.isNewEntity()
                    ? R.string.create_partner
                    : R.string.edit_partner);
            partnerImg.setImageResource(R.drawable.partners_button);
        } else {
            getSupportActionBar().setTitle(mPartner.isNewEntity()
                    ? R.string.create_supplier
                    : R.string.edit_supplier);
            partnerImg.setImageResource(R.drawable.suppliers_button);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.modify_delete_menu, menu);
        ActivityUtils.colorMenuItem(menu.findItem(R.id.back_menu_item), Color.BLACK);
        ActivityUtils.colorMenuItem(menu.findItem(R.id.delete_menu_item), Color.BLACK);
        ActivityUtils.colorMenuItem(menu.findItem(R.id.save_menu_item), Color.BLACK);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.back_menu_item) {
            finish();
            return true;
        }

        if (item.getItemId() == R.id.delete_menu_item) {
            deletePartner();
            return true;
        }

        if (item.getItemId() == R.id.save_menu_item) {
            savePartner();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void savePartner() {
        boolean fieldsRequired = checkRequiredFields();
        if (!fieldsRequired) {
            return;
        }
        setPartner();
        mPartnerRepository.savePartner(mPartner, new Callback() {
            @Override
            public void onSuccess(Object object) {
                finish();
            }

            @Override
            public void onError(Object object) {
                ActivityUtils.alert(ModifyPartnersSuppliersActivity.this, R.string.partner_save_error);
            }
        });
    }

    private boolean checkRequiredFields() {
        if (identifierEt.getText().length() == 0 || nameEt.getText().length() == 0) {
            ActivityUtils.alert(this, R.string.partner_save_constraint_msg);
            return false;
        }
        return true;
    }


    private void setPartner() {
        mPartner.setPartnerIdentifier(identifierEt.getText().toString());
        mPartner.setName(nameEt.getText().toString());
        mPartner.setTaxNumber(taxNumberEt.getText().toString());
        mPartner.setAddress(addressEt.getText().toString());
        mPartner.setPhoneNumber(phoneNumberEt.getText().toString());
        mPartner.setEmail(emailEt.getText().toString());
    }

    private void deletePartner() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.delete_partner_msg)
                .setTitle(R.string.delete)
                .setCancelable(false)
                .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                })
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        mPartnerRepository.deletePartner(mPartner, new Callback() {
                            @Override
                            public void onSuccess(Object object) {
                                finish();
                            }

                            @Override
                            public void onError(Object object) {
                                ActivityUtils.alert(ModifyPartnersSuppliersActivity.this, R.string.partner_delete_error);
                            }
                        });
                    }
                });

        builder.create().show();
    }
}