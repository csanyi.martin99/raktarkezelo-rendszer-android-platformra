package com.example.raktarkezelo.activity;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;

import com.example.raktarkezelo.R;
import com.example.raktarkezelo.data.Partner;
import com.example.raktarkezelo.data.Product;
import com.example.raktarkezelo.data.StockMovement;
import com.example.raktarkezelo.data.StockView;
import com.example.raktarkezelo.data.Storage;
import com.example.raktarkezelo.service.Callback;
import com.example.raktarkezelo.service.PartnerRepository;
import com.example.raktarkezelo.service.PartnerRepositoryImpl;
import com.example.raktarkezelo.service.WarehouseRepository;
import com.example.raktarkezelo.service.WarehouseRepositoryImpl;
import com.example.raktarkezelo.util.ActivityUtils;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import static com.example.raktarkezelo.data.StockMovement.MovementDirection.INWARD;
import static com.example.raktarkezelo.data.StockMovement.MovementDirection.OUTWARD;
import static com.example.raktarkezelo.service.FirebaseReference.AUTHENTICATION;

public class StockMovementDetailActivity extends AppCompatActivity {

    private static final String LOG_TAG = StockMovementDetailActivity.class.getName();

    private StockMovement mStockMovement;
    private List<Product> mProductList = new ArrayList<>();
    private Product selectedProduct;
    private List<Storage> mStorageList = new ArrayList<>();
    private Storage selectedStorage;
    private List<Partner> mPartnerList = new ArrayList<>();
    private Partner selectedPartner;
    private WarehouseRepository warehouseRepository;
    private PartnerRepository partnerRepository;
    private DateFormat mDateFormat;
    private Date selectedStockMovementDate;
    private DecimalFormat mDecimalFormat;

    private ArrayAdapter<Product> productAdapter;
    private ArrayAdapter<Storage> storageAdapter;
    private ArrayAdapter<Partner> partnerAdapter;
    private SearchableSpinner productSpinner;
    private Spinner storageSpinner;
    private SearchableSpinner partnerSpinner;
    private Switch smDirectionSwitch;
    private TextView incomeTv, releaseTv, smStorageTv, smStorageValueTv, smPartnerTv,
            smPartnerValueTv, smTimeTv, smProductTv, smProductValueTv,
            smBarcodeTv, smBarcodeValueTv, currentStockTv, currentStockValueTv, smQuantityTv;
    private EditText smTimeEt, smQuantityEt;
    private Button barcodeButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stock_movement_detail);
        ActivityUtils.validateUser(this, AUTHENTICATION.getCurrentUser());

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        warehouseRepository = new WarehouseRepositoryImpl(this);
        partnerRepository = new PartnerRepositoryImpl(this);
        mDecimalFormat = new DecimalFormat("###,###.##");
        mDecimalFormat.setRoundingMode(RoundingMode.HALF_UP);


        bindViewComponents();
        loadOrCreateStockMovement();
        setViewComponentsVisibility();
        initStorageSpinner();
        setDirectionSwitchListener();
        initPartnerSpinner();
        initProductSpinner();
        initDatePickers();
        if (mStockMovement.isSaved()) {
            setCurrentStockValue(mStockMovement.getProduct(), mStockMovement.getStorage());
        }
    }

    private void bindViewComponents() {
        productSpinner = findViewById(R.id.productSpinner);
        storageSpinner = findViewById(R.id.storageSpinner);
        partnerSpinner = findViewById(R.id.partnerSpinner);

        smDirectionSwitch = findViewById(R.id.smDirectionSwitch);

        incomeTv = findViewById(R.id.incomeTv);
        releaseTv = findViewById(R.id.releaseTv);
        smStorageTv = findViewById(R.id.smStorageTv);
        smStorageValueTv = findViewById(R.id.smStorageValueTv);
        smPartnerTv = findViewById(R.id.smPartnerTv);
        smPartnerValueTv = findViewById(R.id.smPartnerValueTv);
        smTimeTv = findViewById(R.id.smTimeTv);
        smProductTv = findViewById(R.id.smProductTv);
        smProductValueTv = findViewById(R.id.smProductValueTv);
        smBarcodeTv = findViewById(R.id.smBarcodeTv);
        smBarcodeValueTv = findViewById(R.id.smBarcodeValueTv);
        currentStockTv = findViewById(R.id.currentStockTv);
        currentStockValueTv = findViewById(R.id.currentStockValueTv);
        smQuantityTv = findViewById(R.id.smQuantityTv);

        smTimeEt = findViewById(R.id.smTimeEt);
        smQuantityEt = findViewById(R.id.smQuantityEt);

        barcodeButton = findViewById(R.id.barcodeButton);
    }

    private void loadOrCreateStockMovement() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            mStockMovement = (StockMovement) extras.get(StockMovementActivity.PARAM_STOCK_MOVEMENT);
        }
        if (mStockMovement == null) {
            mStockMovement = new StockMovement();
        } else {
            loadStockMovement();
        }
    }

    private void loadStockMovement() {
        warehouseRepository.getStockMovementById(mStockMovement.getId(), new Callback() {
            @Override
            public void onSuccess(Object object) {
                if (object != null) {
                    mStockMovement = (StockMovement) object;
                    setViewComponentsValue();
                }
            }

            @Override
            public void onError(Object object) {
                Log.e(LOG_TAG, "Nem sikerült lekérdezni a készletmozgást!");
            }
        });
    }

    private void setViewComponentsValue() {
        smDirectionSwitch.setChecked(OUTWARD.equals(mStockMovement.getMovementDirection()));
        smPartnerTv.setText(smDirectionSwitch.isChecked() ? R.string.partner : R.string.supplier);
        smStorageValueTv.setText(mStockMovement.getStorage().getStorageName());
        smPartnerValueTv.setText(mStockMovement.getPartner().getNameWithRegNumber());
        smTimeEt.setText(mDateFormat.format(mStockMovement.getMovementDate()));
        Product product = mStockMovement.getProduct();
        smBarcodeValueTv.setText(product.getBarcode());
        smProductValueTv.setText(product.getNameWithProductNumber());

        StringBuilder sb = new StringBuilder();
        sb.append(mDecimalFormat.format(mStockMovement.getQuantity()));
        sb.append(" ");
        sb.append(mStockMovement.getProduct().getUnit());
        smQuantityEt.setText(sb.toString());
    }

    private void setCurrentStockValue(Product product, Storage storage) {
        if (product == null || storage == null) {
            return;
        }
        warehouseRepository.getStockByStorageProductAndDate(storage, product, new Date(), new Callback() {
            @Override
            public void onSuccess(Object object) {
                if (object == null) {
                    return;
                }
                List<StockView> stockList = (List<StockView>) object;

                StringBuilder sb = new StringBuilder();
                if (stockList.isEmpty()) {
                    sb.append(mDecimalFormat.format(BigDecimal.ZERO));
                } else {
                    sb.append(mDecimalFormat.format(stockList.get(0).getQuantity()));
                }
                if (product.getUnit() != null) {
                    sb.append(" ");
                    sb.append(product.getUnit());
                }
                currentStockValueTv.setText(sb.toString());
            }

            @Override
            public void onError(Object object) {
                Log.e(LOG_TAG, "Nem sikerült lekérdezni a készletet!");
            }
        });
    }

    private void setViewComponentsVisibility() {
        smTimeEt.setInputType(InputType.TYPE_NULL);
        if (mStockMovement.isSaved()) {
            smDirectionSwitch.setClickable(false);
            storageSpinner.setVisibility(View.GONE);
            partnerSpinner.setVisibility(View.GONE);
            productSpinner.setVisibility(View.GONE);
            smQuantityEt.setInputType(InputType.TYPE_NULL);
            barcodeButton.setVisibility(View.GONE);
        } else {
            smStorageValueTv.setVisibility(View.GONE);
            smPartnerValueTv.setVisibility(View.GONE);
            smProductValueTv.setVisibility(View.GONE);
        }

        getSupportActionBar().setTitle(mStockMovement.isNewEntity()
                ? R.string.create_stock_movement
                : R.string.edit_stock_movement);
    }


    private void initStorageSpinner() {
        storageAdapter = new ArrayAdapter<>(
                StockMovementDetailActivity.this,
                android.R.layout.simple_spinner_dropdown_item,
                mStorageList);
        storageSpinner.setAdapter(storageAdapter);
        storageSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedStorage = (Storage) parent.getItemAtPosition(position);
                setCurrentStockValue(selectedProduct, selectedStorage);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        loadStorages();
    }

    private void loadStorages() {
        mStorageList.clear();
        warehouseRepository.listAllStorage(new Callback() {
            @Override
            public void onSuccess(Object object) {
                if (object != null) {
                    mStorageList.addAll((List<Storage>) object);
                }
                storageAdapter.notifyDataSetChanged();
            }

            @Override
            public void onError(Object object) {
                Log.e(LOG_TAG, "Nem sikerült lekérdezni a raktárakat!");
            }
        });
    }

    private void initPartnerSpinner() {
        partnerAdapter = new ArrayAdapter<>(
                StockMovementDetailActivity.this,
                android.R.layout.simple_spinner_dropdown_item,
                mPartnerList);
        partnerSpinner.setAdapter(partnerAdapter);
        partnerSpinner.setPositiveButton(getString(R.string.close));
        partnerSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedPartner = (Partner) parent.getItemAtPosition(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        loadPartners();
    }

    private void loadPartners() {
        mPartnerList.clear();
        Partner.PartnerType partnerType = smDirectionSwitch.isChecked()
                ? Partner.PartnerType.CUSTOMER
                : Partner.PartnerType.SUPPLIER;
        partnerRepository.findAllPartner(partnerType, new Callback() {
            @Override
            public void onSuccess(Object object) {
                if (object != null) {
                    mPartnerList.addAll((List<Partner>) object);
                }
                partnerAdapter.notifyDataSetChanged();
                partnerSpinner.setTitle(Partner.PartnerType.CUSTOMER.equals(partnerType)
                        ? getString(R.string.partner_spinner_message)
                        : getString(R.string.supplier_spinner_message));
                smPartnerTv.setText(smDirectionSwitch.isChecked() ? R.string.partner : R.string.supplier);
            }

            @Override
            public void onError(Object object) {
                Log.e(LOG_TAG, "Nem sikerült lekérdezni a partnereket!");
            }
        });
    }

    private void initProductSpinner() {
        productAdapter = new ArrayAdapter<>(
                StockMovementDetailActivity.this,
                android.R.layout.simple_spinner_dropdown_item,
                mProductList);
        productSpinner.setAdapter(productAdapter);
        productSpinner.setTitle(getString(R.string.product_spinner_message));
        productSpinner.setPositiveButton(getString(R.string.close));
        productSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedProduct = (Product) parent.getItemAtPosition(position);
                setCurrentStockValue(selectedProduct, selectedStorage);
                if (selectedProduct.getBarcode() != null) {
                    smBarcodeValueTv.setText(selectedProduct.getBarcode());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        loadProducts();
    }

    private void loadProducts() {
        mProductList.clear();
        warehouseRepository.listAllProduct(new Callback() {
            @Override
            public void onSuccess(Object object) {
                if (object != null) {
                    mProductList.addAll((List<Product>) object);
                }
                productAdapter.notifyDataSetChanged();
            }

            @Override
            public void onError(Object object) {
                Log.e(LOG_TAG, "Nem sikerült lekérdezni a termékeket!");
            }
        });
    }

    private void initDatePickers() {
        mDateFormat = new SimpleDateFormat("yyyy.MM.dd. hh:mm");
        if (mStockMovement.isSaved()) {
            return;
        }

        Calendar calendar = Calendar.getInstance();
        TimePickerDialog timePickerDialog = new TimePickerDialog(
                this,
                new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        Calendar dateCalendar = Calendar.getInstance();
                        dateCalendar.setTime(selectedStockMovementDate);
                        Calendar dateTimeCalendar = Calendar.getInstance();
                        dateTimeCalendar.set(
                                dateCalendar.get(Calendar.YEAR),
                                dateCalendar.get(Calendar.MONTH),
                                dateCalendar.get(Calendar.DAY_OF_MONTH),
                                hourOfDay,
                                minute,
                                0);
                        smTimeEt.setText(mDateFormat.format(dateTimeCalendar.getTime()));
                        selectedStockMovementDate = dateTimeCalendar.getTime();
                    }
                },
                calendar.get(Calendar.HOUR_OF_DAY),
                calendar.get(Calendar.MINUTE),
                true
        );

        DatePickerDialog datePickerDialog = new DatePickerDialog(
                this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        selectedStockMovementDate = null;
                        Calendar calInstance = Calendar.getInstance();
                        calInstance.set(year, month, dayOfMonth, 0, 0, 0);
                        selectedStockMovementDate = calInstance.getTime();
                        timePickerDialog.show();
                    }
                },
                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH));


        smTimeEt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datePickerDialog.show();
            }
        });
        smTimeEt.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    datePickerDialog.show();
                }
            }
        });

        Date currentDate = new Date();
        smTimeEt.setText(mDateFormat.format(currentDate));
        selectedStockMovementDate = currentDate;
    }

    private void setDirectionSwitchListener() {
        smDirectionSwitch.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                loadPartners();
            }
        });
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        if (mStockMovement != null && mStockMovement.isSaved()) {
            MenuItem saveMenuItem = menu.findItem(R.id.save_menu_item);
            saveMenuItem.setVisible(false);
        } else {
            MenuItem deleteMenuItem = menu.findItem(R.id.delete_menu_item);
            deleteMenuItem.setVisible(false);
        }
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.modify_delete_menu, menu);
        ActivityUtils.colorMenuItem(menu.findItem(R.id.back_menu_item), Color.BLACK);
        ActivityUtils.colorMenuItem(menu.findItem(R.id.delete_menu_item), Color.BLACK);
        ActivityUtils.colorMenuItem(menu.findItem(R.id.save_menu_item), Color.BLACK);
        return true;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.back_menu_item) {
            finish();
            return true;
        }

        if (item.getItemId() == R.id.save_menu_item) {
            saveStockMovement();
            return true;
        }

        if (item.getItemId() == R.id.delete_menu_item) {
            deleteStockMovement();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void saveStockMovement() {
        boolean fieldsRequired = checkRequiredFields();
        if (!fieldsRequired) {
            return;
        }
        setStockMovement();
        warehouseRepository.saveStockMovement(mStockMovement, new Callback() {
            @Override
            public void onSuccess(Object object) {
                finish();
            }

            @Override
            public void onError(Object object) {
                if (!(object instanceof String)) {
                    ActivityUtils.alert(StockMovementDetailActivity.this, R.string.sm_save_error);
                } else {
                    ActivityUtils.alert(StockMovementDetailActivity.this, (String) object);
                }
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private boolean checkRequiredFields() {
        final List<String> missingFields = new ArrayList<>();
        if (selectedStorage == null) {
            missingFields.add("Storage");
        }
        if (selectedPartner == null) {
            missingFields.add(smDirectionSwitch.isChecked() ? "Customer" : "Supplier");
        }
        if (smTimeEt.getText().length() == 0) {
            missingFields.add("Stock movement time");
        }
        if (selectedProduct == null) {
            missingFields.add("Product");
        }
        if (smQuantityEt.getText().length() == 0) {
            missingFields.add("Quantity");
        }

        if (!missingFields.isEmpty()) {
            StringBuilder sb = new StringBuilder();
            sb.append("Fill all the required fields:\n");
            sb.append(missingFields.stream().collect(Collectors.joining("\n")));
            ActivityUtils.alert(this, sb.toString());
            return false;
        }

        return true;
    }

    private void setStockMovement() {
        mStockMovement.setMovementDirection(smDirectionSwitch.isChecked() ? OUTWARD : INWARD);
        mStockMovement.setStorage(selectedStorage);
        mStockMovement.setPartner(selectedPartner);
        mStockMovement.setMovementDate(selectedStockMovementDate);
        mStockMovement.setProduct(selectedProduct);
        mStockMovement.setQuantity(Double.valueOf(smQuantityEt.getText().toString()));
    }

    private void deleteStockMovement() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.delete_stock_movement_msg)
                .setTitle(R.string.delete)
                .setCancelable(false)
                .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                })
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        warehouseRepository.deleteStockMovement(mStockMovement, new Callback() {
                            @Override
                            public void onSuccess(Object object) {
                                finish();
                            }

                            @Override
                            public void onError(Object object) {
                                ActivityUtils.alert(StockMovementDetailActivity.this, R.string.stock_movement_delete_error);
                            }
                        });
                    }
                });

        builder.create().show();
    }

    ActivityResultLauncher<Intent> barcodeScanResultLauncher = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            new ActivityResultCallback<ActivityResult>() {
                @Override
                public void onActivityResult(ActivityResult result) {
                    if (result.getResultCode() == Activity.RESULT_OK) {
                        Intent resultIntent = result.getData();
                        Bundle extras = resultIntent.getExtras();
                        if (extras == null) {
                            return;
                        }
                        Product product = (Product) extras.get(StockListActivity.PARAM_PRODUCT);
                        selectProduct(product);
                    }
                }
            });

    private void selectProduct(Product selectedProductParam) {
        selectedProduct = selectedProductParam;
        int index = mProductList.lastIndexOf(selectedProduct);
        productSpinner.setSelection(index);
    }

    public void scanBarcode(View view) {
        Intent barcodeScanIntent = new Intent(this, CameraActivity.class);
        barcodeScanResultLauncher.launch(barcodeScanIntent);
    }
}