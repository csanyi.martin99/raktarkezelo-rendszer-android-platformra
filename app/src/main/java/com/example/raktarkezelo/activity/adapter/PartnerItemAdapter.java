package com.example.raktarkezelo.activity.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.raktarkezelo.activity.OnItemClickListener;
import com.example.raktarkezelo.R;
import com.example.raktarkezelo.data.Partner;

import java.util.List;

public class PartnerItemAdapter extends RecyclerView.Adapter<PartnerItemAdapter.ViewHolder> {

    private static final String LOG_TAG = PartnerItemAdapter.class.getName();

    private final Activity mContext;
    private final List<Partner> mPartners;
    private OnItemClickListener mOnItemClickListener;

    public PartnerItemAdapter(Activity context, List<Partner> partners, OnItemClickListener onItemClickListener) {
        this.mContext = context;
        this.mPartners = partners;
        this.mOnItemClickListener = onItemClickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext)
                .inflate(R.layout.partner_listitem, parent, false);
        return new ViewHolder(view, mOnItemClickListener);
    }

    //ViewHolder összekötése a Partnerrel
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Partner partner = mPartners.get(position);
        holder.bindTo(partner);
    }

    @Override
    public int getItemCount() {
        return mPartners.size();
    }


    //ViewHolder belső osztály
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView partnerNameTv, partnerIdTv;
        private OnItemClickListener mOnItemClickListener;

        public ViewHolder(@NonNull View itemView, OnItemClickListener listener) {
            super(itemView);

            partnerNameTv = itemView.findViewById(R.id.partnerListitemNameTv);
            partnerIdTv = itemView.findViewById(R.id.partnerListitemIdTv);
            mOnItemClickListener = listener;

            itemView.setOnClickListener(this);
        }

        public void bindTo(Partner partner) {
            partnerNameTv.setText(partner.getName());
            partnerIdTv.setText(partner.getPartnerIdentifier());
        }

        //A ViewHolder figyeli a kattintást.
        //Mivel referenciában megkaptam az 'onItemClickListener'-t megvalósító osztályt (a listázó activityt),
        //így tovább tudom küldeni az eventet pozícióval együtt az activytinek.
        @Override
        public void onClick(View v) {
            mOnItemClickListener.onItemClick(getAdapterPosition());
        }
    }

}
