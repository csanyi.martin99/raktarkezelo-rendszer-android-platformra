package com.example.raktarkezelo.activity;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.example.raktarkezelo.R;
import com.example.raktarkezelo.activity.adapter.StockMovementItemAdapter;
import com.example.raktarkezelo.data.StockMovement;
import com.example.raktarkezelo.service.Callback;
import com.example.raktarkezelo.service.WarehouseRepository;
import com.example.raktarkezelo.service.WarehouseRepositoryImpl;
import com.example.raktarkezelo.util.ActivityUtils;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import static com.example.raktarkezelo.service.FirebaseReference.AUTHENTICATION;

public class StockMovementActivity extends AppCompatActivity implements OnItemClickListener {

    private static final String LOG_TAG = StockMovementActivity.class.getName();
    public static final String PARAM_STOCK_MOVEMENT = "stockMovement";

    private RecyclerView recyclerView;
    private StockMovementItemAdapter adapter;
    private List<StockMovement> mStockMovementList = new ArrayList<>();
    private WarehouseRepository warehouseRepository;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stock_movement);
        ActivityUtils.validateUser(this, AUTHENTICATION.getCurrentUser());

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.stock_movements);

        warehouseRepository = new WarehouseRepositoryImpl(this);
        recyclerView = findViewById(R.id.stockMovementRecyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new StockMovementItemAdapter(this, mStockMovementList, this);
        recyclerView.setAdapter(adapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        loadStockMovements();
    }

    private void loadStockMovements() {
        mStockMovementList.clear();
        warehouseRepository.listAllStockMovement(new Callback() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onSuccess(Object object) {
                if (object != null) {
                    List<StockMovement> stockMovementList = (List<StockMovement>) object;
                    stockMovementList.sort(Comparator.comparing(StockMovement::getMovementDate).reversed());
                    mStockMovementList.addAll(stockMovementList);
                }
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onError(Object object) {
                Log.e(LOG_TAG, "Nem sikerült lekérdezni a készletmozgásokat!");
            }
        });
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.stock_movement_menu, menu);
        ActivityUtils.colorMenuItem(menu.findItem(R.id.back_menu_item), Color.BLACK);
        ActivityUtils.colorMenuItem(menu.findItem(R.id.new_stock_movement_menu_item), Color.BLACK);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.back_menu_item) {
            finish();
            return true;
        }

        if (item.getItemId() == R.id.new_stock_movement_menu_item) {
            Intent intent = new Intent(this, StockMovementDetailActivity.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemClick(int position) {
        StockMovement movement = mStockMovementList.get(position);
        Intent intent = new Intent(this, StockMovementDetailActivity.class);
        intent.putExtra(PARAM_STOCK_MOVEMENT, movement);
        startActivity(intent);
    }

}