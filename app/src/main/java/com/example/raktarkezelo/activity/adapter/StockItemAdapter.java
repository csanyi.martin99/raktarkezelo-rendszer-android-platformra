package com.example.raktarkezelo.activity.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.raktarkezelo.R;
import com.example.raktarkezelo.activity.OnItemClickListener;
import com.example.raktarkezelo.data.StockView;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.List;

public class StockItemAdapter extends RecyclerView.Adapter<StockItemAdapter.ViewHolder> {

    private static final String LOG_TAG = StockItemAdapter.class.getName();

    private final Activity mContext;
    private final List<StockView> mStockList;
    private OnItemClickListener mOnItemClickListener;

    public StockItemAdapter(Activity context, List<StockView> stockList, OnItemClickListener onItemClickListener) {
        this.mContext = context;
        this.mStockList = stockList;
        this.mOnItemClickListener = onItemClickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext)
                .inflate(R.layout.stock_view_listitem, parent, false);
        return new ViewHolder(view, mOnItemClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        StockView stock = mStockList.get(position);
        holder.bindTo(stock);
    }

    @Override
    public int getItemCount() {
        return mStockList.size();
    }


    //ViewHolder inner class
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView productTv;
        private TextView storageTv;
        private TextView quantityTv;
        private OnItemClickListener onItemClickListener;
        private DecimalFormat mDecimalFormatter;

        public ViewHolder(@NonNull View itemView, OnItemClickListener listener) {
            super(itemView);

            productTv = itemView.findViewById(R.id.stockProductTv);
            storageTv = itemView.findViewById(R.id.stockStorageTv);
            quantityTv = itemView.findViewById(R.id.stockQuantityTv);

            onItemClickListener = listener;
            mDecimalFormatter = new DecimalFormat("###,###.####");
            mDecimalFormatter.setRoundingMode(RoundingMode.HALF_UP);

            itemView.setOnClickListener(this);
        }

        public void bindTo(StockView stock) {
            productTv.setText(stock.getProduct().getNameWithProductNumber());
            if (stock.getStorage() != null) {
                storageTv.setText(stock.getStorage().getStorageName());
            }
            StringBuilder sb = new StringBuilder();
            sb.append(mDecimalFormatter.format(stock.getQuantity()));
            sb.append(" ");
            if (stock.getProduct().getUnit() != null) {
                sb.append(stock.getProduct().getUnit());
            }
            quantityTv.setText(sb.toString());
        }

        //A ViewHolder figyeli a kattintást.
        //Mivel referenciában megkaptam az 'onItemClickListener'-t megvalósító osztályt (a listázó activityt),
        //így tovább tudom küldeni az eventet pozícióval együtt az activytinek.
        @Override
        public void onClick(View v) {
            onItemClickListener.onItemClick(getAdapterPosition());
        }
    }

}
