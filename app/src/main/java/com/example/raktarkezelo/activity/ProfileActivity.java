package com.example.raktarkezelo.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.example.raktarkezelo.R;
import com.example.raktarkezelo.data.Firm;
import com.example.raktarkezelo.service.Callback;
import com.example.raktarkezelo.service.PartnerRepository;
import com.example.raktarkezelo.service.PartnerRepositoryImpl;
import com.example.raktarkezelo.util.ActivityUtils;

import static com.example.raktarkezelo.service.FirebaseReference.AUTHENTICATION;

public class ProfileActivity extends AppCompatActivity {

    private static final String LOG_TAG = ProfileActivity.class.getName();
    private static final Integer PROFILE_ID = 1;

    private PartnerRepository mPartnerRepository;
    private Firm mFirm;
    private TextView firmNameValueTv, vatNumberValueTV, firmActivityValueTv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        ActivityUtils.validateUser(this, AUTHENTICATION.getCurrentUser());

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.profile);

        mPartnerRepository = new PartnerRepositoryImpl(this);

        firmNameValueTv = findViewById(R.id.firmNameValueTv);
        vatNumberValueTV = findViewById(R.id.vatNumberValueTV);
        firmActivityValueTv = findViewById(R.id.firmActivityValueTv);

        getFirm();
    }

    private void getFirm() {
        mPartnerRepository.getFirmById(PROFILE_ID, new Callback() {
            @Override
            public void onSuccess(Object object) {
                if (object != null) {
                    mFirm = (Firm) object;
                    setViewComponentsValue();
                }
            }

            @Override
            public void onError(Object object) {
                Log.i(LOG_TAG, "Nem sikerült lekérni a céget!");
            }
        });
    }

    private void setViewComponentsValue() {
        firmNameValueTv.setText(mFirm.getFirmName());
        vatNumberValueTV.setText(mFirm.getVatNumber());
        firmActivityValueTv.setText(mFirm.getFirmActivity());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.back_menu, menu);
        ActivityUtils.colorMenuItem(menu.findItem(R.id.back_menu_item), Color.BLACK);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.back_menu_item) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}