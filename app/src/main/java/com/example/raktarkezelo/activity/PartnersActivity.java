package com.example.raktarkezelo.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;

import com.example.raktarkezelo.R;
import com.example.raktarkezelo.activity.adapter.PartnerItemAdapter;
import com.example.raktarkezelo.data.Partner;
import com.example.raktarkezelo.service.Callback;
import com.example.raktarkezelo.service.PartnerRepository;
import com.example.raktarkezelo.service.PartnerRepositoryImpl;
import com.example.raktarkezelo.util.ActivityUtils;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.List;

import static com.example.raktarkezelo.data.Partner.PartnerType.CUSTOMER;
import static com.example.raktarkezelo.service.FirebaseReference.AUTHENTICATION;

public class PartnersActivity extends AppCompatActivity implements OnItemClickListener {

    public static final String PARAM_PARTNER_TYPE = "partnerType";
    public static final String PARAM_PARTNER = "partner";
    private static final String LOG_TAG = PartnersActivity.class.getName();

    private RecyclerView recyclerView; //view, amiben listázom az elemeket
    private PartnerItemAdapter adapter; //a viewt feltöltöm a Partner listával
    private List<Partner> mPartners = new ArrayList<>();
    private Partner.PartnerType mType = CUSTOMER;
    private PartnerRepository partnerRepository;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_partners);
        ActivityUtils.validateUser(this, AUTHENTICATION.getCurrentUser());

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        partnerRepository = new PartnerRepositoryImpl(this);
        recyclerView = findViewById(R.id.partnerRecyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new PartnerItemAdapter(this, mPartners, this);
        recyclerView.setAdapter(adapter);
    }

    private void loadPartners(Partner.PartnerType type) {
        mPartners.clear();
        partnerRepository.findAllPartner(mType, new Callback() {
            @Override
            public void onSuccess(Object object) {
                if (object != null) {
                    mPartners.addAll((List<Partner>) object);
                }
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onError(Object object) {
                Log.e(LOG_TAG, "Nem sikerült lekérdezni a partnereket!");
            }
        });
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.new_partner_menu_item);
        item.setTitle(CUSTOMER.equals(mType)
                ? R.string.create_partner
                : R.string.create_supplier);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.partners_menu, menu);
        ActivityUtils.colorMenuItem(menu.findItem(R.id.back_menu_item), Color.BLACK);
        ActivityUtils.colorMenuItem(menu.findItem(R.id.new_partner_menu_item), Color.BLACK);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.back_menu_item) {
            finish();
            return true;
        }

        if (item.getItemId() == R.id.new_partner_menu_item) {
            addNewPartner();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void addNewPartner() {
        Intent intent = new Intent(this, ModifyPartnersSuppliersActivity.class);
        intent.putExtra(PARAM_PARTNER_TYPE, mType);
        startActivity(intent);
    }

    //eventListener, ami egy elemre kattintást figyel: átnavigálunk a partner részleteibe
    @Override
    public void onItemClick(int position) {
        Partner partner = mPartners.get(position);
        Intent intent = new Intent(this, ModifyPartnersSuppliersActivity.class);
        intent.putExtra(PARAM_PARTNER, partner);
        intent.putExtra(PARAM_PARTNER_TYPE, mType);
        startActivity(intent);
    }

    //Ha folytatódik az activity, akkor újra lekérem a partner listát, így friss adattal dolgozok.
    @Override
    protected void onResume() {
        super.onResume();
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            mType = (Partner.PartnerType) extras.get(PARAM_PARTNER_TYPE);
        }
        getSupportActionBar().setTitle(CUSTOMER.equals(mType)
                ? R.string.title_activity_partners
                : R.string.title_activity_supplier);
        loadPartners(mType);
    }
}