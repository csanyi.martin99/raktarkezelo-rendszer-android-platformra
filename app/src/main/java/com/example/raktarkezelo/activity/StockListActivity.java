package com.example.raktarkezelo.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;

import com.example.raktarkezelo.R;
import com.example.raktarkezelo.activity.adapter.StockItemAdapter;
import com.example.raktarkezelo.data.StockView;
import com.example.raktarkezelo.service.Callback;
import com.example.raktarkezelo.service.WarehouseRepository;
import com.example.raktarkezelo.service.WarehouseRepositoryImpl;
import com.example.raktarkezelo.util.ActivityUtils;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.List;

import static com.example.raktarkezelo.service.FirebaseReference.AUTHENTICATION;

public class StockListActivity extends AppCompatActivity implements OnItemClickListener {

    private static final String LOG_TAG = StockListActivity.class.getName();
    public static final String PARAM_PRODUCT = "product";

    private RecyclerView recyclerView;
    private StockItemAdapter adapter;
    private List<StockView> mStockList = new ArrayList<>();
    private WarehouseRepository warehouseRepository;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stock_list);
        ActivityUtils.validateUser(this, AUTHENTICATION.getCurrentUser());

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.list_of_stock);

        warehouseRepository = new WarehouseRepositoryImpl(this);
        recyclerView = findViewById(R.id.stockRecyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new StockItemAdapter(this, mStockList, this);
        recyclerView.setAdapter(adapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        loadStock();
    }

    private void loadStock() {
        mStockList.clear();
        warehouseRepository.listAllStock(new Callback() {
            @Override
            public void onSuccess(Object object) {
                if (object != null) {
                    mStockList.addAll((List<StockView>) object);
                }
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onError(Object object) {
                Log.e(LOG_TAG, "Nem sikerült lekérdezni a készletet!");
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.stock_menu, menu);
        ActivityUtils.colorMenuItem(menu.findItem(R.id.back_menu_item), Color.BLACK);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.back_menu_item) {
            finish();
            return true;
        }

        if (item.getItemId() == R.id.new_product_menu_item) {
            Intent intent = new Intent(this, ProductDetailActivity.class);
            startActivity(intent);
            return true;
        }
        if (item.getItemId() == R.id.new_stock_movement_menu_item) {
            Intent intent = new Intent(this, StockMovementDetailActivity.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemClick(int position) {
        StockView stock = mStockList.get(position);
        Intent intent = new Intent(this, ProductDetailActivity.class);
        intent.putExtra(PARAM_PRODUCT, stock.getProduct());
        startActivity(intent);
    }
}