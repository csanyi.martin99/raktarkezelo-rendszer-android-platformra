package com.example.raktarkezelo.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.example.raktarkezelo.R;
import com.example.raktarkezelo.data.Partner;
import com.example.raktarkezelo.util.ActivityUtils;

import static com.example.raktarkezelo.service.FirebaseReference.AUTHENTICATION;

public class HomepageActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_homepage);
        ActivityUtils.validateUser(this, AUTHENTICATION.getCurrentUser());
    }

    public void logoffButton(View view) {
        AUTHENTICATION.signOut();
        finish();
    }

    public void warehouseButton(View view) {
        Intent raktarMenuIntent = new Intent(this, WarehouseActivity.class);
        startActivity(raktarMenuIntent);
    }

    public void supplierButton(View view) {
        Intent suppliersIntent = new Intent(this, PartnersActivity.class);
        suppliersIntent.putExtra(PartnersActivity.PARAM_PARTNER_TYPE, Partner.PartnerType.SUPPLIER);
        startActivity(suppliersIntent);
    }

    public void partnersButton(View view) {
        Intent partnersIntent = new Intent(this, PartnersActivity.class);
        partnersIntent.putExtra(PartnersActivity.PARAM_PARTNER_TYPE, Partner.PartnerType.CUSTOMER);
        startActivity(partnersIntent);
    }

    public void profileButton(View view) {
        Intent profileIntent = new Intent(this, ProfileActivity.class);
        startActivity(profileIntent);
    }
}