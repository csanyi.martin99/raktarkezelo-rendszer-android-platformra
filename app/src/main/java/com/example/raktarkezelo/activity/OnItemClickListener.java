package com.example.raktarkezelo.activity;

//Listener interfész, amivel az elemre kattintást lekezelem.
//Az interfészt a listázó oldalak implementálják,
//ott van tényleges logika, hogy mi történjen a kattintás hatására.
public interface OnItemClickListener {
    void onItemClick(int position);
}
