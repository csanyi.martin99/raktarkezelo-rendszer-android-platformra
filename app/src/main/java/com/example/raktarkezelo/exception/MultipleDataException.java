package com.example.raktarkezelo.exception;

public class MultipleDataException extends IllegalArgumentException {

    private final Object recurringData;

    public MultipleDataException(String s, Object recurringData) {
        super(s);
        this.recurringData = recurringData;
    }

    public Object getRecurringData() {
        return recurringData;
    }
}
