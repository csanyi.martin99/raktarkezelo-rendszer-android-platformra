package com.example.raktarkezelo.service;

public abstract class Callback {
    public abstract void  onSuccess(Object object);
    public abstract void  onError(Object object);
}
