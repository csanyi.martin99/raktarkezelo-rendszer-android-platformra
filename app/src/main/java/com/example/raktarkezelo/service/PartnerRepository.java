package com.example.raktarkezelo.service;

import com.example.raktarkezelo.data.Partner;

public interface PartnerRepository {

    void savePartner(Partner partner, Callback callback);

    void deletePartner(Partner partner, Callback callback);

    void findAllPartner(Partner.PartnerType type, Callback callBack);

    void getPartnerById(Integer id, Callback callback);

    void getFirmById(Integer id, Callback callback);
}
