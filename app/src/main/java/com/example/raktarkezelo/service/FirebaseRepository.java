package com.example.raktarkezelo.service;

import androidx.annotation.NonNull;

import com.example.raktarkezelo.data.TrackedEntity;
import com.example.raktarkezelo.exception.MultipleDataException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.example.raktarkezelo.service.FirebaseReference.DATABASE;

public abstract class FirebaseRepository {

    protected final void fireStoreCreate(
            final DocumentReference documentReference, final Object model, final Callback callback) {
        documentReference.set(model).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                callback.onSuccess("SUCCESS");
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                callback.onError(e);
            }
        });
    }

    protected final void readQueryDocuments(final Query query, final Callback callback) {
        query.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    QuerySnapshot querySnapshot = task.getResult();
                    if (querySnapshot != null && !querySnapshot.isEmpty()) {
                        callback.onSuccess(querySnapshot);
                    } else {
                        callback.onSuccess(null);
                    }
                } else {
                    callback.onError(task.getException());
                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                callback.onError(e);
            }
        });
    }

    public <T extends TrackedEntity> void saveNewEntity(T entity, Callback callback) {
        CollectionReference referenceEntity = DATABASE.collection(entity.getClass().getSimpleName());

        //ha új entity-t hozok létre, akkor le kell kérnem az utolsó id-t
        Task<QuerySnapshot> snapshot = referenceEntity.limit(1)
                .orderBy("created", Query.Direction.DESCENDING)
                .get();
        snapshot.addOnSuccessListener(queryDocumentSnapshots -> {
            Integer sequenceId = getNextId(queryDocumentSnapshots.getDocuments());
            String id = String.valueOf(sequenceId);
            entity.setId(sequenceId);
            entity.setCreated(new Date());
            callback.onSuccess(referenceEntity.document(id));
        });
    }

    //az utolsó id-t növelem 1-el, ez lesz az új id
    private Integer getNextId(List<DocumentSnapshot> documents) {
        if (documents.isEmpty()) {
            return 1;
        }
        Integer id = Integer.parseInt(documents.get(0).getId());
        return ++id;
    }

    public <T extends TrackedEntity> DocumentReference refreshEntity(T entity, boolean delete) {
        DocumentReference reference = DATABASE
                .collection(entity.getClass().getSimpleName())
                .document(String.valueOf(entity.getId()));
        if (delete) {
            entity.setDeleted(new Date());
        } else {
            entity.setLastEdit(new Date());
        }
        return reference;
    }

    public <T extends TrackedEntity> DocumentReference deleteEntity(T entity) {
        return refreshEntity(entity, true);
    }

    public <T> List<T> getDataFromQuerySnapshot(Object object, Class<T> clazz) {
        final List<T> dataList = new ArrayList<>();
        QuerySnapshot queryDocumentSnapshots = (QuerySnapshot) object;
        for (DocumentSnapshot snapshot : queryDocumentSnapshots) {
            dataList.add(snapshot.toObject(clazz));
        }
        return dataList;
    }

    public <T> T getSingleDataFromQuerySnapshot(Object object, Class<T> clazz) {
        QuerySnapshot queryDocumentSnapshots = (QuerySnapshot) object;
        List<DocumentSnapshot> documents = queryDocumentSnapshots.getDocuments();
        if (documents.isEmpty()) {
            return null;
        }
        if (documents.size() > 1) {
            throw new MultipleDataException(
                    "Hiba! Id duplikáció: " + documents.get(0).getId(),
                    documents);
        }
        return documents.get(0).toObject(clazz);
    }
}
