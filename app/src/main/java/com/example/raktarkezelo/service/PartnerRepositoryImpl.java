package com.example.raktarkezelo.service;

import android.app.Activity;

import com.example.raktarkezelo.data.Firm;
import com.example.raktarkezelo.data.Partner;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.Query;

public class PartnerRepositoryImpl extends FirebaseRepository implements PartnerRepository {

    private static final String LOG_TAG = PartnerRepositoryImpl.class.getName();

    private Activity activity;
    private CollectionReference partnerCollectionReference;
    private CollectionReference firmCollectionReference;

    public PartnerRepositoryImpl(Activity activity) {
        this.activity = activity;
        partnerCollectionReference = FirebaseReference.DATABASE.collection("Partner");
        firmCollectionReference = FirebaseReference.DATABASE.collection("Firm");
    }

    @Override
    public void savePartner(Partner partner, Callback callback) {
        if (partner.isNewEntity()) {
            createNewPartner(partner, callback);
        } else {
            updatePartner(partner, false, callback);
        }
    }

    @Override
    public void deletePartner(Partner partner, Callback callback) {
        updatePartner(partner, true, callback);
    }

    private void createNewPartner(Partner partner, Callback callback) {
        saveNewEntity(partner, new Callback() {
            @Override
            public void onSuccess(Object object) {
                DocumentReference documentReference = (DocumentReference) object;
                fireStoreCreate(documentReference, partner, new Callback() {
                    @Override
                    public void onSuccess(Object object) {
                        documentReference.update("reference", documentReference.getPath());
                        callback.onSuccess("SUCCESS");
                    }

                    @Override
                    public void onError(Object object) {
                        callback.onError(object);
                    }
                });
            }

            @Override
            public void onError(Object object) {
                callback.onError(object);
            }
        });
    }

    private void updatePartner(Partner partner, boolean isDelete, Callback callback) {
        DocumentReference documentReference = refreshEntity(partner, isDelete);
        fireStoreCreate(documentReference, partner, new Callback() {
            @Override
            public void onSuccess(Object object) {
                callback.onSuccess("SUCCESS");
            }

            @Override
            public void onError(Object object) {
                callback.onError(object);
            }
        });
    }

    @Override
    public void findAllPartner(Partner.PartnerType type, final Callback callback) {
        Query query = partnerCollectionReference
                .whereEqualTo("deleted", null)
                .whereEqualTo("type", type)
                .orderBy("name");
        readQueryDocuments(query, new Callback() {
            @Override
            public void onSuccess(Object object) {
                if (object == null) {
                    callback.onSuccess(null);
                } else
                    callback.onSuccess(getDataFromQuerySnapshot(object, Partner.class));
            }

            @Override
            public void onError(Object object) {
                callback.onError(object);
            }
        });
    }

    @Override
    public void getPartnerById(Integer id, Callback callback) {
        Query query = partnerCollectionReference
                .whereEqualTo("id", id);
        readQueryDocuments(query, new Callback() {
            @Override
            public void onSuccess(Object object) {
                if (object == null) {
                    callback.onSuccess(null);
                } else
                    callback.onSuccess(getSingleDataFromQuerySnapshot(object, Partner.class));
            }

            @Override
            public void onError(Object object) {
                callback.onError(object);
            }
        });
    }

    @Override
    public void getFirmById(Integer id, Callback callback) {
        Query query = firmCollectionReference
                .whereEqualTo("id", id);
        readQueryDocuments(query, new Callback() {
            @Override
            public void onSuccess(Object object) {
                if (object == null) {
                    callback.onSuccess(null);
                } else
                    callback.onSuccess(getSingleDataFromQuerySnapshot(object, Firm.class));
            }

            @Override
            public void onError(Object object) {
                callback.onError(object);
            }
        });
    }


}
