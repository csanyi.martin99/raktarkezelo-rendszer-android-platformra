package com.example.raktarkezelo.service;

import android.app.Activity;
import android.os.Build;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

import com.example.raktarkezelo.activity.handler.NotificationHandler;
import com.example.raktarkezelo.R;
import com.example.raktarkezelo.data.Partner;
import com.example.raktarkezelo.data.Product;
import com.example.raktarkezelo.data.StockMovement;
import com.example.raktarkezelo.data.StockView;
import com.example.raktarkezelo.data.Storage;
import com.example.raktarkezelo.exception.MultipleDataException;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.Query;

import org.jetbrains.annotations.NotNull;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.example.raktarkezelo.service.FirebaseReference.DATABASE;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.reducing;

public class WarehouseRepositoryImpl extends FirebaseRepository implements WarehouseRepository {

    private static final String LOG_TAG = WarehouseRepositoryImpl.class.getName();

    private Activity activity;
    private CollectionReference stockMovementCollectionReference;
    private CollectionReference productCollectionReference;
    private CollectionReference storageCollectionReference;

    public WarehouseRepositoryImpl(Activity activity) {
        this.activity = activity;
        stockMovementCollectionReference = DATABASE.collection("StockMovement");
        productCollectionReference = DATABASE.collection("Product");
        storageCollectionReference = DATABASE.collection("Storage");
    }

    @Override
    public void saveProduct(Product product, Callback callback) {
        if (product.isNewEntity()) {
            createNewProduct(product, callback);
        } else {
            updateProduct(product, false, callback);
        }
    }

    private void createNewProduct(Product product, Callback callback) {
        saveNewEntity(product, new Callback() {
            @Override
            public void onSuccess(Object object) {
                DocumentReference documentReference = (DocumentReference) object;
                fireStoreCreate(documentReference, product, new Callback() {
                    @Override
                    public void onSuccess(Object object) {
                        documentReference.update("reference", documentReference.getPath());
                        callback.onSuccess("SUCCESS");
                    }

                    @Override
                    public void onError(Object object) {
                        callback.onError(object);
                    }
                });
            }

            @Override
            public void onError(Object object) {
                callback.onError(object);
            }
        });
    }

    private void updateProduct(Product product, boolean isDelete, Callback callback) {
        DocumentReference documentReference = refreshEntity(product, isDelete);
        fireStoreCreate(documentReference, product, new Callback() {
            @Override
            public void onSuccess(Object object) {
                if (!isDelete) {
                    callback.onSuccess("SUCCESS");
                    return;
                }
                listStockMovementsByProduct(product, new Callback() {
                    @Override
                    public void onSuccess(Object object) {
                        if (object == null) {
                            callback.onError("ERROR");
                            return;
                        }
                        List<StockMovement> movements = (List<StockMovement>) object;
                        for (StockMovement movement : movements) {
                            deleteStockMovement(movement, new Callback() {
                                @Override
                                public void onSuccess(Object object) {
                                    callback.onSuccess("SUCCESS");
                                }

                                @Override
                                public void onError(Object object) {
                                    Log.e(LOG_TAG, object.toString());
                                    callback.onError(object);
                                }
                            });
                        }
                    }

                    @Override
                    public void onError(Object object) {
                        Log.e(LOG_TAG, object.toString());
                        callback.onError(object);
                    }
                });
            }

            @Override
            public void onError(Object object) {
                callback.onError(object);
            }
        });
    }

    @Override
    public void deleteProduct(Product product, Callback callback) {
        updateProduct(product, true, callback);
    }

    @Override
    public void getProductById(Integer id, Callback callback) {
        Query query = productCollectionReference
                .whereEqualTo("id", id);
        readQueryDocuments(query, new Callback() {
            @Override
            public void onSuccess(Object object) {
                if (object == null) {
                    callback.onSuccess(null);
                } else
                    callback.onSuccess(getSingleDataFromQuerySnapshot(object, Product.class));
            }

            @Override
            public void onError(Object object) {
                callback.onError(object);
            }
        });
    }

    public void getProductByBarcode(String barcode, Callback callback) {
        Query query = productCollectionReference
                .whereEqualTo("barcode", barcode)
                .whereEqualTo("deleted", null);
        readQueryDocuments(query, new Callback() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onSuccess(Object object) {
                if (object == null) {
                    callback.onSuccess(null);
                    return;
                }
                try {
                    callback.onSuccess(getSingleDataFromQuerySnapshot(object, Product.class));
                } catch (MultipleDataException ex) {
                    callback.onError(getErrorMessage(ex));
                }
            }

            @RequiresApi(api = Build.VERSION_CODES.N)
            private String getErrorMessage(MultipleDataException ex) {
                List<DocumentSnapshot> recurringDataList = (List<DocumentSnapshot>) ex.getRecurringData();
                List<Product> productList = new ArrayList<>();
                recurringDataList.stream().forEach(doc -> productList.add(doc.toObject(Product.class)));
                String repeatedProducts = productList.stream()
                        .map(product -> product.toString())
                        .collect(Collectors.joining(", "));
                StringBuilder sb = new StringBuilder();
                sb.append("Multiple product has the same (");
                sb.append(barcode);
                sb.append(") barcode: ");
                sb.append(repeatedProducts);
                return sb.toString();
            }

            @Override
            public void onError(Object object) {
                callback.onError(object);
            }
        });
    }

    @Override
    public void listAllProduct(Callback callback) {
        Query query = productCollectionReference
                .whereEqualTo("deleted", null)
                .orderBy("name", Query.Direction.ASCENDING);
        readQueryDocuments(query, new Callback() {
            @Override
            public void onSuccess(Object object) {
                if (object == null) {
                    callback.onSuccess(null);
                } else {
                    List<Product> productList = getDataFromQuerySnapshot(object, Product.class);
                    callback.onSuccess(productList);
                }
            }

            @Override
            public void onError(Object object) {
                Log.e(LOG_TAG, object.toString());
                callback.onError(object);
            }
        });
    }

    @Override
    public void listAllStorage(Callback callback) {
        Query query = storageCollectionReference
                .whereEqualTo("deleted", null)
                .orderBy("storageName", Query.Direction.ASCENDING);
        readQueryDocuments(query, new Callback() {
            @Override
            public void onSuccess(Object object) {
                if (object == null) {
                    callback.onSuccess(null);
                } else {
                    List<Storage> storageList = getDataFromQuerySnapshot(object, Storage.class);
                    callback.onSuccess(storageList);
                }
            }

            @Override
            public void onError(Object object) {
                Log.e(LOG_TAG, object.toString());
                callback.onError(object);
            }
        });
    }

    @Override
    public void listAllStock(Callback callback) {
        Query query = stockMovementCollectionReference
                .whereEqualTo("deleted", null)
                .orderBy("movementDate");
        readQueryDocuments(query, new Callback() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onSuccess(Object object) {
                if (object == null) {
                    callback.onSuccess(object);
                    return;
                }
                getStockMovements(object, new Callback() {
                    @Override
                    public void onSuccess(Object object) {
                        List<StockMovement> stockMovementList = (List<StockMovement>) object;
                        calculateStockListWithEmptyStock(stockMovementList, callback);
                    }

                    @Override
                    public void onError(Object object) {
                        callback.onError(object);
                    }
                });

            }

            @RequiresApi(api = Build.VERSION_CODES.N)
            private void calculateStockListWithEmptyStock(List<StockMovement> stockMovementList, Callback callback) {
                listAllProduct(new Callback() {
                    @Override
                    public void onSuccess(Object object) {
                        List<Product> productList = object == null
                                ? new ArrayList<>()
                                : (List<Product>) object;
                        List<Product> productsWithStock = stockMovementList.stream()
                                .map(StockMovement::getProduct)
                                .distinct()
                                .collect(Collectors.toList());
                        List<Product> productsWithoutStock = productList.stream()
                                .filter(p -> !productsWithStock.contains(p))
                                .collect(Collectors.toList());

                        List<StockView> stockList = new ArrayList<>();
                        Map<StockMovement.GroupingKey, BigDecimal> groupedQuantity = stockMovementList.stream()
                                .collect(groupingBy(StockMovement::getGroupingKey,
                                        reducing(BigDecimal.ZERO, StockMovement::getQuantityBigDecimal, BigDecimal::add)));

                        for (Map.Entry<StockMovement.GroupingKey, BigDecimal> entry : groupedQuantity.entrySet()) {
                            StockMovement.GroupingKey key = entry.getKey();
                            BigDecimal quantity = entry.getValue();
                            stockList.add(new StockView(new Date(), key.getStorage(), key.getProduct(), quantity));
                        }

                        for (Product product : productsWithoutStock) {
                            stockList.add(new StockView(new Date(), null, product, BigDecimal.ZERO));
                        }

                        callback.onSuccess(stockList);
                    }

                    @Override
                    public void onError(Object object) {
                        Log.e(LOG_TAG, object.toString());
                        callback.onError(object);
                    }
                });
            }

            @Override
            public void onError(Object object) {
                Log.e(LOG_TAG, object.toString());
                callback.onError(object);
            }
        });
    }

    @Override
    public void getStockByStorageProductAndDate(Storage storage, Product product, Date date, Callback callback) {
        Query query = stockMovementCollectionReference
                .whereEqualTo("storageReference", storage.getReference())
                .whereEqualTo("productReference", product.getReference())
                .whereLessThanOrEqualTo("movementDate", date)
                .whereEqualTo("deleted", null)
                .orderBy("movementDate");
        readQueryDocuments(query, new Callback() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onSuccess(Object object) {
                List<StockView> stockList = new ArrayList<>();
                if (object == null) {
                    callback.onSuccess(stockList);
                    return;
                }

                getStockMovements(object, new Callback() {
                    @Override
                    public void onSuccess(Object object) {
                        List<StockMovement> stockMovementList = (List<StockMovement>) object;
                        if (stockMovementList.isEmpty()) {
                            callback.onSuccess(stockList);
                            return;
                        }

                        BigDecimal sumQuantity = stockMovementList.stream()
                                .map(StockMovement::getQuantityBigDecimal)
                                .reduce(BigDecimal.ZERO, BigDecimal::add);
                        stockList.add(new StockView(new Date(), storage, product, sumQuantity));
                        callback.onSuccess(stockList);
                    }

                    @Override
                    public void onError(Object object) {
                        Log.e(LOG_TAG, object.toString());
                        callback.onError(object);
                    }
                });
            }

            @Override
            public void onError(Object object) {
                Log.e(LOG_TAG, object.toString());
                callback.onError(object);
            }
        });
    }

    @Override
    public void saveStockMovement(StockMovement stockMovement, Callback callback) {
        getStockByStorageProductAndDate(stockMovement.getStorage(),
                stockMovement.getProduct(), stockMovement.getMovementDate(), new Callback() {
                    @RequiresApi(api = Build.VERSION_CODES.N)
                    @Override
                    public void onSuccess(Object object) {
                        if (object == null) {
                            return;
                        }
                        boolean validQuantity = validateMovementQuantity((List<StockView>) object);
                        if (!validQuantity) {
                            return;
                        }

                        if (stockMovement.isNewEntity()) {
                            createNewStockMovement(stockMovement, callback);
                        } else {
                            updateStockMovement(stockMovement, false, callback);
                        }

                        checkMinimumMaximumStock((List<StockView>) object, stockMovement, false);
                    }

                    @RequiresApi(api = Build.VERSION_CODES.N)
                    private boolean validateMovementQuantity(List<StockView> stockList) {
                        if (StockMovement.MovementDirection.INWARD.equals(stockMovement.getMovementDirection())) {
                            return true;
                        }
                        BigDecimal stockQuantity = stockList.stream()
                                .map(StockView::getQuantity)
                                .reduce(BigDecimal.ZERO, BigDecimal::add);
                        BigDecimal reducedQuantity = stockQuantity.add(stockMovement.getQuantityBigDecimal());
                        if (reducedQuantity.signum() == -1) {
                            DecimalFormat df = new DecimalFormat("###,###.##");
                            df.setRoundingMode(RoundingMode.HALF_UP);
                            callback.onError(activity.getString(R.string.invalid_quantity_msg, df.format(reducedQuantity)));
                            return false;
                        }
                        return true;
                    }

                    @Override
                    public void onError(Object object) {
                        Log.e(LOG_TAG, object.toString());
                        callback.onError(object);
                    }
                });
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void checkMinimumMaximumStock(List<StockView> stockList, StockMovement stockMovement, boolean delete) {
        DecimalFormat df = new DecimalFormat("###,###.##");
        df.setRoundingMode(RoundingMode.HALF_UP);
        BigDecimal stockQuantity = stockList.stream()
                .map(StockView::getQuantity)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
        BigDecimal newQuantity = delete
                ? stockQuantity.subtract(stockMovement.getQuantityBigDecimal())
                : stockQuantity.add(stockMovement.getQuantityBigDecimal());
        Product product = stockMovement.getProduct();
        if (product.getMinimumStock() != null) {
            if (newQuantity.compareTo(product.getMinimumStockBigDecimal()) < 0) {
                NotificationHandler handler = new NotificationHandler(activity);
                String title = activity.getString(R.string.minimum_stock);
                String message = activity.getString(R.string.minimum_stock_message,
                        product.getNameWithProductNumber(),
                        df.format(newQuantity),
                        df.format(product.getMinimumStock()));
                handler.send(title, message);
            }
        }

        if (product.getMaximumStock() != null) {
            if (newQuantity.compareTo(product.getMaximumStockBigDecimal()) > 0) {
                NotificationHandler handler = new NotificationHandler(activity);
                String title = activity.getString(R.string.maximum_stock);
                String message = activity.getString(R.string.maximum_stock_message,
                        product.getNameWithProductNumber(),
                        df.format(newQuantity),
                        df.format(product.getMaximumStock()));
                handler.send(title, message);
            }
        }
    }

    private void createNewStockMovement(StockMovement stockMovement, Callback callback) {
        saveNewEntity(stockMovement, new Callback() {
            @Override
            public void onSuccess(Object object) {
                DocumentReference documentReference = (DocumentReference) object;
                fireStoreCreate(documentReference, stockMovement, new Callback() {
                    @Override
                    public void onSuccess(Object object) {
                        callback.onSuccess("SUCCESS");
                    }

                    @Override
                    public void onError(Object object) {
                        callback.onError(object);
                    }
                });
            }

            @Override
            public void onError(Object object) {
                callback.onError(object);
            }
        });
    }

    private void updateStockMovement(StockMovement stockMovement, boolean isDelete, Callback callback) {
        DocumentReference documentReference = refreshEntity(stockMovement, isDelete);
        fireStoreCreate(documentReference, stockMovement, new Callback() {
            @Override
            public void onSuccess(Object object) {
                getStockByStorageProductAndDate(stockMovement.getStorage(),
                        stockMovement.getProduct(), new Date(), new Callback() {
                            @RequiresApi(api = Build.VERSION_CODES.N)
                            @Override
                            public void onSuccess(Object object) {
                                callback.onSuccess("SUCCESS");
                                if (object == null) {
                                    return;
                                }
                                checkMinimumMaximumStock((List<StockView>) object, stockMovement, true);
                            }

                            @Override
                            public void onError(Object object) {
                                Log.e(LOG_TAG, object.toString());
                                callback.onError(object);
                            }
                        });
            }

            @Override
            public void onError(Object object) {
                callback.onError(object);
            }
        });
    }

    @Override
    public void deleteStockMovement(StockMovement stockMovement, Callback callback) {
        updateStockMovement(stockMovement, true, callback);
    }

    @Override
    public void listAllStockMovement(Callback callback) {
        Query query = stockMovementCollectionReference
                .whereEqualTo("deleted", null)
                .orderBy("movementDate", Query.Direction.DESCENDING);
        readQueryDocuments(query, new Callback() {
            @Override
            public void onSuccess(Object object) {
                if (object == null) {
                    callback.onSuccess(null);
                } else {
                    getStockMovements(object, callback);
                }
            }

            @Override
            public void onError(Object object) {
                Log.e(LOG_TAG, object.toString());
                callback.onError(object);
            }
        });
    }

    private void getStockMovements(Object object, Callback callback) {
        List<StockMovement> stockMovementList = getDataFromQuerySnapshot(object, StockMovement.class);
        List<StockMovement> referencedStockMovementList = new ArrayList<>();
        for (StockMovement movement : stockMovementList) {
            setStorageReference(movement, new Callback() {
                @Override
                public void onSuccess(Object object) {
                    referencedStockMovementList.add((StockMovement) object);
                    if (referencedStockMovementList.size() == stockMovementList.size()) {
                        callback.onSuccess(referencedStockMovementList);
                    }
                }

                @Override
                public void onError(Object object) {
                    callback.onError(object);
                }
            });
        }
    }

    private void listStockMovementsByProduct(Product product, Callback callback) {
        Query query = stockMovementCollectionReference
                .whereEqualTo("productReference", product.getReference())
                .whereEqualTo("deleted", null)
                .orderBy("movementDate", Query.Direction.DESCENDING);
        readQueryDocuments(query, new Callback() {
            @Override
            public void onSuccess(Object object) {
                if (object == null) {
                    callback.onSuccess(null);
                } else {
                    getStockMovements(object, callback);
                }
            }

            @Override
            public void onError(Object object) {
                Log.e(LOG_TAG, object.toString());
                callback.onError(object);
            }
        });
    }

    @Override
    public void getStockMovementById(Integer id, Callback callback) {
        Query query = stockMovementCollectionReference
                .whereEqualTo("id", id);
        readQueryDocuments(query, new Callback() {
            @Override
            public void onSuccess(Object object) {
                if (object == null) {
                    callback.onSuccess(null);
                } else {
                    StockMovement stockMovement = getSingleDataFromQuerySnapshot(object, StockMovement.class);
                    setStorageReference(stockMovement, callback);
                }
            }

            @Override
            public void onError(Object object) {
                callback.onError(object);
            }
        });
    }


    private void setStorageReference(StockMovement stockMovement, Callback callback) {
        DocumentReference storageReference = DATABASE.document(stockMovement.getStorageReference());
        if (storageReference != null) {
            storageReference.get()
                    .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                        @Override
                        public void onSuccess(@NonNull @NotNull DocumentSnapshot documentSnapshot) {
                            stockMovement.setStorage(documentSnapshot.toObject(Storage.class));
                            setProductReference(stockMovement, callback);
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull @NotNull Exception e) {
                            callback.onError(e);
                        }
                    });
        }
    }

    private void setProductReference(StockMovement stockMovement, Callback callback) {
        DocumentReference productReference = DATABASE.document(stockMovement.getProductReference());
        if (productReference != null) {
            productReference.get()
                    .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                        @Override
                        public void onSuccess(@NonNull @NotNull DocumentSnapshot documentSnapshot) {
                            stockMovement.setProduct(documentSnapshot.toObject(Product.class));
                            setPartnerReference(stockMovement, callback);
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull @NotNull Exception e) {
                            callback.onError(e);
                        }
                    });
        }
    }

    private void setPartnerReference(StockMovement stockMovement, Callback callback) {
        DocumentReference productReference = DATABASE.document(stockMovement.getPartnerReference());
        if (productReference != null) {
            productReference.get()
                    .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                        @Override
                        public void onSuccess(@NonNull @NotNull DocumentSnapshot documentSnapshot) {
                            stockMovement.setPartner(documentSnapshot.toObject(Partner.class));
                            callback.onSuccess(stockMovement);
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull @NotNull Exception e) {
                            callback.onError(e);
                        }
                    });
        }
    }
}
