package com.example.raktarkezelo.service;

import com.example.raktarkezelo.data.Product;
import com.example.raktarkezelo.data.StockMovement;
import com.example.raktarkezelo.data.Storage;

import java.util.Date;

public interface WarehouseRepository {

    void saveProduct(Product product, Callback callback);

    void deleteProduct(Product product, Callback callback);

    void getProductById(Integer id, Callback callback);

    void getProductByBarcode(String barcode, Callback callback);

    void listAllProduct(Callback callback);

    void listAllStorage(Callback callback);

    void listAllStock(Callback callback);

    void getStockByStorageProductAndDate(Storage storage, Product product, Date date, Callback callback);

    void saveStockMovement(StockMovement stockMovement, Callback callback);

    void deleteStockMovement(StockMovement stockMovement, Callback callback);

    void listAllStockMovement(Callback callback);

    void getStockMovementById(Integer id, Callback callback);


}
