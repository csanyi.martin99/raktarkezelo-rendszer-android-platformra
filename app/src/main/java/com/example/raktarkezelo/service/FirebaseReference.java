package com.example.raktarkezelo.service;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;

public class FirebaseReference {

    private FirebaseReference() {
    }

    public static final FirebaseFirestore DATABASE = FirebaseFirestore.getInstance();
    public static final FirebaseAuth AUTHENTICATION = FirebaseAuth.getInstance();
}
